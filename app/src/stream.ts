import {NestFactory} from "@nestjs/core";
import {AppModule} from "./app/app.module";
import {AppController} from "./app/app.controller";

async function bootstrap() {

    const app = await NestFactory.create(AppModule);
    const appController = app.get(AppController) as AppController;
    appController.startCandleStreamService();
}

// noinspection JSIgnoredPromiseFromCall
bootstrap();