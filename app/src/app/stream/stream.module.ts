import {Module} from "@nestjs/common";
import {StreamService} from "./stream.service";
import {CustomLogger} from "@cryptex-labs/common";
import {CandleDataModule} from "../../domain/candle/data/candle-data.module";
import {CandleModule} from "../../domain/candle/candle.module";
import {CandleStreamConsumerModule} from "../../domain/candle/stream/consumer/candle-stream-consumer.module";

const loggerProvider = {
    provide:  "LoggerService",
    useValue: new CustomLogger("info"),
};

@Module({
    imports:   [
        CandleModule,
        CandleStreamConsumerModule,
        CandleDataModule,
        StreamModule,
    ],
    providers: [loggerProvider, StreamService],
    exports:   [StreamService],
})
export class StreamModule {
}