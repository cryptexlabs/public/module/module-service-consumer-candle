import {Inject, Injectable, LoggerService} from "@nestjs/common";
import {CandleDataService} from "../../domain/candle/data/candle-data.service";
import {CandleDataInterface} from "@cryptex-labs/common-candle";
import {StreamServiceStateMachine} from "./state-machine/stream-service-state-machine";
import {CandleService} from "../../domain/candle/candle.service";
import {CandleStreamConsumerService} from "../../domain/candle/stream/consumer/candle-stream-consumer.service";
import {TradeUtil} from "@cryptex-labs/common-trade";
import {CandlePairConfig} from "../../domain/candle/stream/consumer/candle-pair.config";

@Injectable()
export class StreamService {

    private stateMachine: StreamServiceStateMachine;

    constructor(@Inject("LoggerService") private readonly logger: LoggerService,
                @Inject("CandleStreamConsumerConfigs") private readonly config: CandlePairConfig,
                private readonly candleConsumerService: CandleStreamConsumerService,
                private readonly candleDataService: CandleDataService,
                private readonly candleService: CandleService) {

        this.stateMachine = new StreamServiceStateMachine(
            logger,
            candleDataService,
            candleService,
        );
    }

    public start() {

        this.loadHistoricalCandles()
            .then(() => {
                return this.candleConsumerService.consume((candle: CandleDataInterface, exchange: string, asset: string) => {
                    const delay = (new Date()).getTime() - candle.getLastTrades()[0].getTime();
                    this.logger.log("Trade ids: " + TradeUtil.getTradeIdsForList(candle.getLastTrades()) + " Delay: " + delay);
                    this.stateMachine.onCandleEvent(candle, exchange, asset)
                        .catch((error) => {
                            this.logger.error("Error: " + error);
                        });
                });
            })
            .catch((error) => {
                this.logger.error(error);
            });
    }

    private loadHistoricalCandles(): Promise<any> {

        const promises = [];

        for (const config of this.config.getConfigs()) {
            promises.push(Promise.resolve());
        }

        return Promise.all(promises);
    }

}