import {StreamServiceStateInterface} from "./stream-service-state.interface";
import {StreamServiceStateMachine} from "./stream-service-state-machine";
import {CandleDataInterface} from "@cryptex-labs/common-candle";

export class StreamServiceInitializingState implements StreamServiceStateInterface {

    constructor(private readonly context: StreamServiceStateMachine) {
    }

    public onCandleEvent(candle: CandleDataInterface, exchange: string, asset: string): Promise<any> {
        this.context.postponeEvent(candle, exchange, asset);
        return Promise.resolve();
    }
}