
import {CandleDataInterface} from "@cryptex-labs/common-candle";

export interface  StreamServiceStateInterface {
    onCandleEvent(candle: CandleDataInterface, exchange: string, asset: string): Promise<any>;
}