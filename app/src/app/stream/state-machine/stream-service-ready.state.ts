import {StreamServiceStateInterface} from "./stream-service-state.interface";
import {StreamServiceStateMachine} from "./stream-service-state-machine";
import {CandleDataInterface} from "@cryptex-labs/common-candle";

export class StreamServiceReadyState implements StreamServiceStateInterface {

    constructor(private readonly context: StreamServiceStateMachine) {
        this.context = context;
    }

    public onCandleEvent(candle: CandleDataInterface, exchange: string, asset: string) {
        this.context.publishEvent(candle, exchange, asset);
        return Promise.resolve();
    }
}