import {StreamServiceStateInterface} from "./stream-service-state.interface";
import {StreamServiceStateMachine} from "./stream-service-state-machine";
import {LoggerService} from "@nestjs/common";
import {CandleService} from "../../../domain/candle/candle.service";
import {CandleDataInterface} from "@cryptex-labs/common-candle";
import {CandleDataService} from "../../../domain/candle/data/candle-data.service";

export class StreamServiceDefaultState implements StreamServiceStateInterface {

    constructor(private readonly context: StreamServiceStateMachine,
                private readonly logger: LoggerService,
                private readonly candleDataService: CandleDataService,
                private readonly candleService: CandleService) {
    }

    public onCandleEvent(candle: CandleDataInterface, exchange: string, asset: string): Promise<any> {
        this.context.setState(this.context.getInitializingState());

        return this.publishMissingTrades(candle, exchange, asset)
            .then(() => {
                this.context.publishEvent(candle, exchange, asset);
            })
            .then(() => {
                this.publishPostponedEvents(exchange, asset);
            })
            .then(() => {
                this.context.setState(this.context.getReadyState());
            });
    }

    private publishMissingTrades(currentCandle: CandleDataInterface, exchange: string, asset: string): Promise<any> {
        return this.getMissingTrades(currentCandle)
            .then((candles: CandleDataInterface[]) => {
                this.publishTrades(candles, exchange, asset);
            })
            .catch((error) => {
                this.logger.error(error);
            });
    }

    private publishPostponedEvents(exchange: string, asset: string) {
        this.publishTrades(this.context.getPostponedEvents(), exchange, asset);
    }

    private publishTrades(candles: CandleDataInterface[], exchange: string, asset: string) {
        for (const candle of candles) {
            this.context.publishEvent(candle, exchange, asset);
        }
    }

    private getMissingTrades(currentCandle: CandleDataInterface): Promise<CandleDataInterface[]> {
        const lastTimeForCandles = this.candleDataService.getLatestTimeForCandles();
        if (lastTimeForCandles > 0) {
            return this.candleService.getCandlesBetweenRange(lastTimeForCandles, currentCandle.getLastTrades()[0].getTime());
        } else {
            return Promise.resolve([]);
        }
    }
}