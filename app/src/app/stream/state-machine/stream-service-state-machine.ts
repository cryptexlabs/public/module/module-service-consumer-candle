import {StreamServiceReadyState} from "./stream-service-ready.state";
import {StreamServiceInitializingState} from "./stream-service-initializing.state";
import {StreamServiceStateInterface} from "./stream-service-state.interface";
import {StreamServiceDefaultState} from "./stream-service-default.state";
import {LoggerService} from "@nestjs/common";
import {CandleDataService} from "../../../domain/candle/data/candle-data.service";
import {CandleService} from "../../../domain/candle/candle.service";
import {CandleDataInterface} from "@cryptex-labs/common-candle";

export class StreamServiceStateMachine implements StreamServiceStateInterface {

    private defaultState: StreamServiceStateInterface;
    private initializingState: StreamServiceStateInterface;
    private readyState: StreamServiceStateInterface;
    private currentState: StreamServiceStateInterface;

    private postponedEvents: CandleDataInterface[];

    constructor(private readonly logger: LoggerService,
                private readonly candleDataService: CandleDataService,
                private readonly candleService: CandleService) {

        this.defaultState      = new StreamServiceDefaultState(this, logger, candleDataService, candleService);
        this.initializingState = new StreamServiceInitializingState(this);
        this.readyState        = new StreamServiceReadyState(this);
        this.currentState      = this.defaultState;
        this.postponedEvents   = [];
    }

    public setState(state: StreamServiceStateInterface) {
        this.currentState = state;
    }

    public onCandleEvent(candle: CandleDataInterface, exchange: string, asset: string): Promise<any> {
        return this.currentState.onCandleEvent(candle, exchange, asset);
    }

    public postponeEvent(candle: CandleDataInterface, exhange: string, asset: string) {
        this.postponedEvents.push(candle);
    }

    public publishEvent(candle: CandleDataInterface, exchange: string, asset: string) {
        this.persistCandle(candle, exchange, asset);
    }

    private persistCandle(candle: CandleDataInterface, exchange: string, asset: string) {
        this.candleDataService.saveCandles([candle], exchange, asset).catch((error) => {
            this.logger.error("StreamServiceStateMachine.persistCandle:48 " + error);
        });
    }

    public getPostponedEvents(): CandleDataInterface[] {
        return this.postponedEvents;
    }

    public getDefaultState(): StreamServiceStateInterface {
        return this.defaultState;
    }

    public getInitializingState(): StreamServiceStateInterface {
        return this.initializingState;
    }

    public getReadyState(): StreamServiceStateInterface {
        return this.readyState;
    }

}