import {Module} from "@nestjs/common";
import {AppController} from "./app.controller";
import {CustomLogger} from "@cryptex-labs/common";
import {StreamModule} from "./stream/stream.module";

const loggerProvider = {
    provide:  "LoggerService",
    useValue: new CustomLogger("info"),
};

@Module({
    imports:     [StreamModule],
    controllers: [AppController],
    providers:   [loggerProvider],
})
export class AppModule {
}