import {Controller, Inject, LoggerService} from "@nestjs/common";
import {StreamService} from "./stream/stream.service";

@Controller()
export class AppController {

    constructor(@Inject("LoggerService") readonly logger: LoggerService,
                private readonly streamService: StreamService) {
    }

    public startCandleStreamService() {
        this.logger.log("Starting Candle Stream Consumer");
        this.streamService.start();
    }

}
