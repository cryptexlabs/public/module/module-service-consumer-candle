import {Module} from "@nestjs/common";
import {CandleService} from "./candle.service";
import {CustomLogger} from "@cryptex-labs/common";

const loggerProvider = {
    provide:  "LoggerService",
    useValue: new CustomLogger("info"),
};

@Module({
    providers: [CandleService, loggerProvider],
    exports: [CandleService],
})
export class CandleModule {}