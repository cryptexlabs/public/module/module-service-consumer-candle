import {CandleConsumerModuleExchangesConfigInterface} from "../interfaces/candle.consumer-module.exchanges-config.interface";
import {CandleConsumerModuleExchangeConfigInterface} from "../interfaces/candle.consumer-module.exchange-config.interface";
import {CandleConsumerModuleExchangesConfigRawInterface} from "../raw/candle.consumer-module.exchanges-config-raw.interface";
import {CandleConsumerModuleExchangeConfig} from "./candle.consumer-module.exchange-config";
import {CandleConsumerModuleCandleConfigConfig} from "./candle.consumer-module.candle-config-config";

export class CandleConsumerModuleExchangesConfig implements CandleConsumerModuleExchangesConfigInterface {

    private readonly _exchanges: CandleConsumerModuleExchangeConfigInterface[];

    constructor(rawConfig: CandleConsumerModuleExchangesConfigRawInterface) {

        this._exchanges        = [];
        const defaultRawConfig = rawConfig.candle ? rawConfig.candle.config : null;
        const defaultConfig    = new CandleConsumerModuleCandleConfigConfig(defaultRawConfig, null);

        for (const rawExchange of rawConfig.exchanges) {
            if (typeof rawExchange === "string") {
                this._exchanges.push(new CandleConsumerModuleExchangeConfig(
                    {
                        id:     rawExchange,
                        candle: rawConfig.candle,
                    }, defaultConfig,
                ));
            } else {
                this._exchanges.push(new CandleConsumerModuleExchangeConfig(rawExchange, defaultConfig));
            }
        }
    }

    public getExchanges(): CandleConsumerModuleExchangeConfigInterface[] {
        return this._exchanges;
    }
}