import {CandleConsumerModuleExchangeConfigInterface} from "../interfaces/candle.consumer-module.exchange-config.interface";
import {CandleConsumerModuleCandleConfigInterface} from "../interfaces/candle.consumer-module.candle-config.interface";
import {CandleConsumerModuleExchangeConfigRawInterface} from "../raw/candle.consumer-module.exchange-config-raw.interface";
import {CandleConsumerModuleCandleConfigConfigInterface} from "../interfaces/candle.consumer-module.candle-config-config.interface";
import {CandleConsumerModuleCandleConfig} from "./candle.consumer-module.candle-config";

export class CandleConsumerModuleExchangeConfig implements CandleConsumerModuleExchangeConfigInterface {

    private readonly _candle: CandleConsumerModuleCandleConfigInterface;

    constructor(
        private readonly rawConfig: CandleConsumerModuleExchangeConfigRawInterface,
        private readonly defaultConfig: CandleConsumerModuleCandleConfigConfigInterface) {
        this._candle = new CandleConsumerModuleCandleConfig(rawConfig.candle, defaultConfig);
    }

    public getId(): string {
        return this.rawConfig.id;
    }

    public getCandle(): CandleConsumerModuleCandleConfigInterface {
        return this._candle;
    }
}