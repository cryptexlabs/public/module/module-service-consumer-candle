import {CandleConsumerModulePairConfigInterface} from "../interfaces/candle.consumer-module.pair-config.interface";
import {CandleConsumerModuleGranularityConfigInterface} from "../interfaces/candle.consumer-module.granularity-config.interface";
import {CandleConsumerModulePairConfigRawInterface} from "../raw/candle.consumer-module.pair-config-raw.interface";
import {CandleConsumerModuleCandleConfigConfigInterface} from "../interfaces/candle.consumer-module.candle-config-config.interface";
import {CandleConsumerModuleGranularityConfig} from "./candle.consumer-module.granularity-config";

export class CandleConsumerModulePairConfig implements CandleConsumerModulePairConfigInterface {

    private readonly _granularities: CandleConsumerModuleGranularityConfigInterface;

    constructor(private readonly rawConfig: CandleConsumerModulePairConfigRawInterface,
                private readonly defaultConfig: CandleConsumerModuleCandleConfigConfigInterface) {

        if (rawConfig.granularities) {
            this._granularities = new CandleConsumerModuleGranularityConfig(rawConfig.granularities, defaultConfig.getGranularityConfig());
        } else if (defaultConfig !== null && defaultConfig.getGranularityConfig()) {
            this._granularities = defaultConfig.getGranularityConfig();
        } else {
            throw Error("Invalid config. Missing granularities");
        }
    }

    public getGranularityConfig(): CandleConsumerModuleGranularityConfigInterface {
        return this._granularities;
    }

    public getId(): string {
        return this.rawConfig.id;
    }

}