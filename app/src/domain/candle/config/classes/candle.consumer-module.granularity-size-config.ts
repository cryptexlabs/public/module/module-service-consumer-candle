import {CandleConsumerModuleGranularitySizeConfigInterface} from "../interfaces/candle.consumer-module.granularity-size-config.interface";
import {CandleConsumerModuleGranularitySizeConfigRawInterface} from "../raw/candle.consumer-module.granularity-size-config-raw.interface";

export class CandleConsumerModuleGranularitySizeConfig implements CandleConsumerModuleGranularitySizeConfigInterface {

    constructor(
        private readonly rawConfig?: CandleConsumerModuleGranularitySizeConfigRawInterface,
        defaultConfig?: CandleConsumerModuleGranularitySizeConfigInterface,
    ) {
        if ((!rawConfig || !rawConfig.length) && (!defaultConfig || !defaultConfig.getLength())) {
            throw Error("Missing length");
        }
        if ((!rawConfig || !rawConfig.size) && (!defaultConfig || !defaultConfig.getSize())) {
            throw Error("Missing size");
        }
    }

    public getLength(): number {
        return this.rawConfig.length;
    }

    public getSize(): number {
        return this.rawConfig.size;
    }

}