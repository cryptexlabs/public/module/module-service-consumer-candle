import {CandleConsumerModuleGranularityConfigInterface} from "../interfaces/candle.consumer-module.granularity-config.interface";
import {CandleConsumerModuleGranularitySizeConfigInterface} from "../interfaces/candle.consumer-module.granularity-size-config.interface";
import {CandleConsumerModuleGranularityConfigRawInterface} from "../raw/candle.consumer-module.granularity-config-raw.interface";
import {CandleConsumerModuleGranularitySizeConfig} from "./candle.consumer-module.granularity-size-config";
import {CandleConsumerModuleGranularitySizeConfigRawInterface} from "../raw/candle.consumer-module.granularity-size-config-raw.interface";

export class CandleConsumerModuleGranularityConfig implements CandleConsumerModuleGranularityConfigInterface {

    private readonly _sizes: CandleConsumerModuleGranularitySizeConfigInterface[];

    constructor(rawConfig: CandleConsumerModuleGranularityConfigRawInterface,
                defaultConfig?: CandleConsumerModuleGranularityConfigInterface) {

        this._sizes = [];

        for (const rawSize of rawConfig.sizes) {
            if (!Number.isNaN(Number(rawSize)) && Number.isInteger(Number(rawSize))) {

                const defaultSize = this._getSizeForSize(Number(rawSize), defaultConfig ? defaultConfig.getSizes() : []);
                let defaultLength = defaultSize ? defaultSize.getLength() : null;

                if (!defaultLength && defaultConfig && defaultConfig.getSizes().length > 0) {
                    defaultLength = defaultConfig.getSizes()[0].getLength();
                }

                this._sizes.push(new CandleConsumerModuleGranularitySizeConfig({
                    size:   Number(rawSize),
                    length: rawConfig.length ? rawConfig.length : defaultLength,
                }, defaultSize));
            } else {
                const useSize = rawSize as CandleConsumerModuleGranularitySizeConfigRawInterface;
                this._sizes.push(new CandleConsumerModuleGranularitySizeConfig(
                    useSize, this._getSizeForSize(
                        useSize.size,
                        defaultConfig ? defaultConfig.getSizes() : [],
                    )));
            }
        }
    }

    private _getSizeForSize(
        sizeSize: number,
        sizes: CandleConsumerModuleGranularitySizeConfigInterface[]): CandleConsumerModuleGranularitySizeConfigInterface {
        for (const size of sizes) {
            if (sizeSize === size.getSize()) {
                return size;
            }
        }
        return null;
    }

    public getSizes(): CandleConsumerModuleGranularitySizeConfigInterface[] {
        return this._sizes;
    }
}