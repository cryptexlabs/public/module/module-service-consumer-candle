import {CandleConsumerModuleCandleConfigConfigInterface} from "../interfaces/candle.consumer-module.candle-config-config.interface";
import {CandleConsumerModuleGranularityConfigInterface} from "../interfaces/candle.consumer-module.granularity-config.interface";
import {CandleConsumerModuleCandleConfigConfigRawInterface} from "../raw/candle.consumer-module.candle-config-config-raw.interface";
import {CandleConsumerModuleGranularityConfig} from "./candle.consumer-module.granularity-config";

export class CandleConsumerModuleCandleConfigConfig implements CandleConsumerModuleCandleConfigConfigInterface {

    private readonly _granularityConfig: CandleConsumerModuleGranularityConfigInterface;

    constructor(rawConfig?: CandleConsumerModuleCandleConfigConfigRawInterface,
                defaultConfig?: CandleConsumerModuleCandleConfigConfigInterface,
    ) {
        const granularityConfig = defaultConfig ? defaultConfig.getGranularityConfig() : null;
        if (rawConfig) {
            this._granularityConfig = new CandleConsumerModuleGranularityConfig(
                rawConfig.granularities,
                granularityConfig,
            );
        } else if (defaultConfig) {
            this._granularityConfig = defaultConfig.getGranularityConfig();
        } else {
            this._granularityConfig = null;
        }
    }

    public getGranularityConfig(): CandleConsumerModuleGranularityConfigInterface {
        return this._granularityConfig;
    }
}