import {CandleConsumerModuleCandleConfigInterface} from "../interfaces/candle.consumer-module.candle-config.interface";
import {CandleConsumerModulePairConfigInterface} from "../interfaces/candle.consumer-module.pair-config.interface";
import {CandleConsumerModuleCandleConfigRawInterface} from "../raw/candle.consumer-module.candle-config-raw.interface";
import {CandleConsumerModuleCandleConfigConfig} from "./candle.consumer-module.candle-config-config";
import {CandleConsumerModuleCandleConfigConfigInterface} from "../interfaces/candle.consumer-module.candle-config-config.interface";
import {CandleConsumerModulePairConfig} from "./candle.consumer-module.pair-config";

export class CandleConsumerModuleCandleConfig implements CandleConsumerModuleCandleConfigInterface {

    private readonly _pairs: CandleConsumerModulePairConfigInterface[];

    constructor(rawConfig: CandleConsumerModuleCandleConfigRawInterface, defaultConfig: CandleConsumerModuleCandleConfigConfigInterface) {

        const defaultChildConfig = new CandleConsumerModuleCandleConfigConfig(rawConfig.config, defaultConfig);

        this._pairs = [];
        for (const rawPair of rawConfig.pairs) {
            if (typeof rawPair === "string") {
                this._pairs.push(new CandleConsumerModulePairConfig({
                    id:            rawPair,
                    granularities: null,
                }, defaultChildConfig));
            } else {
                this._pairs.push(new CandleConsumerModulePairConfig(rawPair, defaultChildConfig));
            }
        }
    }

    public getPairs(): CandleConsumerModulePairConfigInterface[] {
        return this._pairs;
    }

}