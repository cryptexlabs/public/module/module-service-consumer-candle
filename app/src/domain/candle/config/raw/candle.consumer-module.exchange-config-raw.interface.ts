import {ExchangeConfigRawInterface} from "@cryptex-labs/common";
import {CandleConsumerModuleCandleConfigRawInterface} from "./candle.consumer-module.candle-config-raw.interface";

export interface CandleConsumerModuleExchangeConfigRawInterface extends ExchangeConfigRawInterface {
    candle: CandleConsumerModuleCandleConfigRawInterface;
}