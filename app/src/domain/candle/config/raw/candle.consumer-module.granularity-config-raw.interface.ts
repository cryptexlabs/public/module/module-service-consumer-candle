import {CandleConsumerModuleGranularitySizeConfigRawInterface} from "./candle.consumer-module.granularity-size-config-raw.interface";

export interface CandleConsumerModuleGranularityConfigRawInterface {
    length?: number;
    sizes: number[] | CandleConsumerModuleGranularitySizeConfigRawInterface[];
}