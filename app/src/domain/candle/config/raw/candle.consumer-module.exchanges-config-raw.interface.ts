import {ExchangesConfigRawInterface} from "@cryptex-labs/common";
import {CandleConsumerModuleExchangeConfigRawInterface} from "./candle.consumer-module.exchange-config-raw.interface";
import {CandleConsumerModuleCandleConfigRawInterface} from "./candle.consumer-module.candle-config-raw.interface";

export interface CandleConsumerModuleExchangesConfigRawInterface extends ExchangesConfigRawInterface {
    exchanges: string[] | CandleConsumerModuleExchangeConfigRawInterface[];
    candle?: CandleConsumerModuleCandleConfigRawInterface;
}