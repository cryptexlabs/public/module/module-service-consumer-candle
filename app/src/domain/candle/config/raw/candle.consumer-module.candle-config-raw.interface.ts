import {CandleConsumerModulePairConfigRawInterface} from "./candle.consumer-module.pair-config-raw.interface";
import {CandleConsumerModuleCandleConfigConfigRawInterface} from "./candle.consumer-module.candle-config-config-raw.interface";

export interface CandleConsumerModuleCandleConfigRawInterface {
    pairs?: string[] | CandleConsumerModulePairConfigRawInterface[];
    config?: CandleConsumerModuleCandleConfigConfigRawInterface;
}