import {CandleConsumerModuleGranularityConfigRawInterface} from "./candle.consumer-module.granularity-config-raw.interface";

export interface CandleConsumerModuleCandleConfigConfigRawInterface {
    granularities: CandleConsumerModuleGranularityConfigRawInterface;
}