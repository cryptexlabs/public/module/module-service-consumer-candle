export interface CandleConsumerModuleGranularitySizeConfigRawInterface {
    size: number;
    length: number;
}