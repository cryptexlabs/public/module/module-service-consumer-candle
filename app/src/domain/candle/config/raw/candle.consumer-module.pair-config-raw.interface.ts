import {CandleConsumerModuleGranularityConfigRawInterface} from "./candle.consumer-module.granularity-config-raw.interface";

export interface CandleConsumerModulePairConfigRawInterface {
    id: string;
    granularities: CandleConsumerModuleGranularityConfigRawInterface;
}