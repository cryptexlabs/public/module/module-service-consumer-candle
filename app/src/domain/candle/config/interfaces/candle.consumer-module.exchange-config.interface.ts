import {ExchangeConfigInterface} from "@cryptex-labs/common";
import {CandleConsumerModuleCandleConfigInterface} from "./candle.consumer-module.candle-config.interface";

export interface CandleConsumerModuleExchangeConfigInterface extends ExchangeConfigInterface {
    getCandle(): CandleConsumerModuleCandleConfigInterface;
}