import {ExchangesConfigInterface} from "@cryptex-labs/common";
import {CandleConsumerModuleExchangeConfigInterface} from "./candle.consumer-module.exchange-config.interface";

export interface CandleConsumerModuleExchangesConfigInterface extends ExchangesConfigInterface {
    getExchanges(): CandleConsumerModuleExchangeConfigInterface[];
}