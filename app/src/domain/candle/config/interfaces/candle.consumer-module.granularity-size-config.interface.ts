export interface CandleConsumerModuleGranularitySizeConfigInterface {
    getSize(): number;
    getLength(): number;
}