import {CandleConsumerModuleGranularitySizeConfigInterface} from "./candle.consumer-module.granularity-size-config.interface";

export interface CandleConsumerModuleGranularityConfigInterface {
    getSizes(): CandleConsumerModuleGranularitySizeConfigInterface[];
}