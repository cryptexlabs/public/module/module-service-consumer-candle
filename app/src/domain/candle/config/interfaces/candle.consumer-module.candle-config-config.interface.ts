import {CandleConsumerModuleGranularityConfigInterface} from "./candle.consumer-module.granularity-config.interface";

export interface CandleConsumerModuleCandleConfigConfigInterface {
    getGranularityConfig(): CandleConsumerModuleGranularityConfigInterface;
}