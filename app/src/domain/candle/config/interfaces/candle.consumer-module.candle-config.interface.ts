import {CandleConsumerModulePairConfigInterface} from "./candle.consumer-module.pair-config.interface";

export interface CandleConsumerModuleCandleConfigInterface {
    getPairs(): CandleConsumerModulePairConfigInterface[];
}