import {CandleConsumerModuleGranularityConfigInterface} from "./candle.consumer-module.granularity-config.interface";

export interface CandleConsumerModulePairConfigInterface {
    getId(): string;
    getGranularityConfig(): CandleConsumerModuleGranularityConfigInterface;
}