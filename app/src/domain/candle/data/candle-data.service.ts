import {Inject, Injectable, LoggerService} from "@nestjs/common";
import {CandleDataConfig} from "./candle-data.config";
import {CandleSerializer, CandleDataInterface} from "@cryptex-labs/common-candle";
import redis = require("redis");
import {TradeDatumSerializer} from "@cryptex-labs/common-trade";

@Injectable()
export class CandleDataService {

    private _redis: any;
    private _isDisconnected: boolean;
    private _restartCount: number;
    private static readonly CANDLE_COUNT = 15;

    private static readonly KEY_NAMESPACE = "candles";

    constructor(@Inject("LoggerService") private readonly logger: LoggerService,
                @Inject("CandleDataConfig") private readonly config: CandleDataConfig) {
        this._restartCount = 0;
        this._initializeRedis();
        this._monitorRedis();
    }

    private _monitorRedis() {
        setInterval(() => {
            if (this._isDisconnected) {
                this._reInitializeRedis();
            }
        }, 1000);
    }

    private _initializeRedis() {
        this._isDisconnected = false;
        this._redis          = redis.createClient(
            this.config.getPort(),
            this.config.getHost(),
        );
        this._redis.on("error", (err) => {
            this.logger.error("Redis error encountered: " + err);
        });
        this._redis.on("end", () => {
            this.logger.warn("Redis connection closed", "warning");
            this._isDisconnected = true;
        });
    }

    private _reInitializeRedis() {
        if (this._restartCount <= 3) {
            this._initializeRedis();
            this._restartCount++;
        } else {
            this.logger.error("Redis reached reconnect max. Exiting");
            process.exit(1);
        }
    }

    public getLatestCandles(granularities: number[]): Promise<CandleDataInterface[]> {
        // TODO Phase-4: CandleDataService.getLatestCandles
        return new Promise((resolve, reject) => {
            resolve([]);
        });
    }

    public getLatestTimeForCandles() {
        // TODO CandleDataService.getLatestTimeForCandles
        return 0;
    }

    public saveCandles(candles: CandleDataInterface[], exchange: string, asset: string): Promise<any> {

        const promises = [];

        for (const candleData of candles) {

            const key = [
                CandleDataService.KEY_NAMESPACE,
                exchange,
                asset,
                candleData.getGranularity(),
            ].join(":");

            promises.push(new Promise((resolve, reject) => {
                this._redis.get(key, (getError, oldRawJsonData) => {

                    if (!getError) {
                        let newRawJsonData;
                        if (oldRawJsonData) {
                            newRawJsonData = this._setOrAddCandle(oldRawJsonData, candleData);
                        } else {
                            newRawJsonData = this._setOrAddCandle("[]", candleData);
                        }

                        this._redis.set(key, newRawJsonData, (setError) => {
                            if (!setError) {
                                resolve();
                            } else {
                                reject(setError);
                            }
                        });
                    } else {
                        reject(getError);
                    }
                });
            }));
        }
        return Promise.all(promises);
    }

    private _setOrAddCandle(oldRawJsonData: string, candleData: CandleDataInterface): string {

        const oldRawData = JSON.parse(oldRawJsonData);

        const data = {
            candle:        new CandleSerializer(candleData.getCandle()),
            lastTrades: (new TradeDatumSerializer(candleData.getLastTrades())).getDatum(),
        };

        if (Array.isArray(oldRawData) && oldRawData.length > 0) {

            const lastOldTimestamp = oldRawData[oldRawData.length - 1].candle.time;

            if (candleData.getCandle().getTime() > lastOldTimestamp) {

                while (oldRawData.length >= CandleDataService.CANDLE_COUNT) {
                    oldRawData.shift();
                }

                this.logger.log("Adding candle: " + JSON.stringify(data));
                oldRawData.push(data);

                return JSON.stringify(oldRawData);
            }

            if (candleData.getCandle().getTime() === lastOldTimestamp) {
                oldRawData[oldRawData.length - 1] = data;
                return JSON.stringify(oldRawData);
            }

            return JSON.stringify(oldRawData);
        } else {
            return JSON.stringify([data]);
        }
    }
}