import {Injectable} from "@nestjs/common";

@Injectable()
export class CandleDataConfig {

    constructor(private readonly host: string,
                private readonly port: number) {

    }

    public getHost(): string {
        return this.host;
    }

    public getPort(): number {
        return this.port;
    }

}