import {Module} from "@nestjs/common";
import {CandleDataService} from "./candle-data.service";
import {CustomLogger} from "@cryptex-labs/common";
import {CandleDataConfig} from "./candle-data.config";

const loggerProvider = {
    provide:  "LoggerService",
    useValue: new CustomLogger("info"),
};

const candleDataConfig = {
    provide:  "CandleDataConfig",
    useValue: new CandleDataConfig(
        process.env.TRADER_REDIS_HOST,
        parseInt(process.env.TRADER_REDIS_PORT, 10),
    ),
};

@Module({
    providers: [CandleDataService, loggerProvider, candleDataConfig],
    exports:   [CandleDataService],
})
export class CandleDataModule {}