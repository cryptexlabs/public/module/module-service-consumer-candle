import {Inject, Injectable, LoggerService} from "@nestjs/common";
import {CandleStreamConsumer} from "./candle-stream-consumer";
import {CandleEventMerger} from "./candle-event-merger";
import {CandleStreamConsumerConfig} from "./candle-stream-consumer.config";
import {CandleDataInterface} from "@cryptex-labs/common-candle";
import {CandleGrpcService} from "@cryptex-labs/nestjs-sdk";

@Injectable()
export class CandleStreamConsumerService {

    private consumers: CandleStreamConsumer[];
    private merger: CandleEventMerger;

    public constructor(@Inject("LoggerService") private readonly logger: LoggerService,
                       @Inject("CandleStreamConsumerConfigs") private readonly configs: CandleStreamConsumerConfig[],
                       private readonly candleService: CandleGrpcService) {
        this.initConsumers(logger, configs);
        this.merger = new CandleEventMerger(logger);
    }

    private initConsumers(logger: LoggerService, configs: CandleStreamConsumerConfig[]) {

        if (configs.length !== 2) {
            this.logger.error("Incorrect config count for CandleStreamConsumerConfig. Expected 2. Found " + configs.length);
            process.exit(1);
        }

        this.consumers = [];
        for (let i = 0; i < configs.length; i++) {
            const config = configs[i];
            this.consumers.push(new CandleStreamConsumer(logger, config, this.candleService, i + 1 as (1 | 2)));
        }
    }

    public consume(onEvent: (candle: CandleDataInterface, exchange: string, asset: string) => any): Promise<any> {
        const promises = [];
        for (const consumer of this.consumers) {
            promises.push(consumer.consume((candle: CandleDataInterface, exchange: string, asset: string) => {
                this.merger.merge(onEvent, candle, exchange, asset);
            }));
        }
        return Promise.all(promises);
    }
}