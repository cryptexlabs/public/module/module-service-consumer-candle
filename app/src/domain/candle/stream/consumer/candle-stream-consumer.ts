import {Injectable, LoggerService} from "@nestjs/common";
import {CandleStreamConsumerConfig} from "./candle-stream-consumer.config";
import {CandleDataInterface} from "@cryptex-labs/common-candle";
import {CandleGrpcService, CandleUpdateInterface} from "@cryptex-labs/nestjs-sdk";

@Injectable()
export class CandleStreamConsumer {

    private service: CandleGrpcService;

    public constructor(private readonly logger: LoggerService,
                       private readonly config: CandleStreamConsumerConfig,
                       private readonly candleService: CandleGrpcService,
                       private readonly clientNumber: 1 | 2) {
    }

    public consume(onEvent: (candleData: CandleDataInterface, exchange: string, asset: string) => any): Promise<any> {
        return this.service.subscribeToCandleUpdates([], this.clientNumber, (update: CandleUpdateInterface) => {
            for (const candleData of update.getCandleDatum()) {
                onEvent(candleData, update.getExchangeId(), update.getPairId());
            }
        });
    }
}