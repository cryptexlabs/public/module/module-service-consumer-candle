import {Injectable} from "@nestjs/common";

@Injectable()
export class CandleStreamConsumerConfig {

    constructor(private readonly host: string, private readonly port: number) {
    }

    public getPort(): number {
        return this.port;
    }

    public getHost(): string {
        return this.host;
    }
}