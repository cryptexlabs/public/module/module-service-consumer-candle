export class CandleConfig {

    constructor(private readonly exchangeKey: string, private readonly assetKey: string, private readonly granularities: string[]) {
    }

    public getExchangeKey() {
        return this.exchangeKey;
    }

    public getGranularities(): string[] {
        return this.granularities;
    }

    public getAssetKey(): string {
        return this.assetKey;
    }
}