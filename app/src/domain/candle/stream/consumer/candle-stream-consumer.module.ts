import {Module} from "@nestjs/common";
import {CustomLogger} from "@cryptex-labs/common";
import {CandleStreamConsumerService} from "./candle-stream-consumer.service";
import {CandleGrpcModule} from "@cryptex-labs/nestjs-sdk";
import {CandlePairConfig} from "./candle-pair.config";

const loggerProvider = {
    provide:  "LoggerService",
    useValue: new CustomLogger("info"),
};

const pairConfig = new CandlePairConfig(
    process.env.MODULE_SERVICE_CONSUMER_CANDLE_CONFIG_FILE_LOCATION,
    loggerProvider.useValue,
);

const candleStreamConsumerConfigs = {
    provide:  "CandleStreamConsumerConfigs",
    useValue: pairConfig.getConfigs(),
};

@Module({
    imports:   [CandleGrpcModule],
    providers: [CandleStreamConsumerService, loggerProvider, candleStreamConsumerConfigs],
    exports:   [CandleStreamConsumerService, candleStreamConsumerConfigs],
})
export class CandleStreamConsumerModule {
}