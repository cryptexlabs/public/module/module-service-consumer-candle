import {LoggerService} from "@nestjs/common";
import {CandleDataInterface} from "@cryptex-labs/common-candle";
import {TradeUtil} from "@cryptex-labs/common-trade";

export class CandleEventMerger {

    private _currentTradeIds: string[][];

    private static readonly BUFFER_SIZE = 1000;

    public constructor(private readonly logger: LoggerService) {
        this._currentTradeIds = [];
    }

    public merge(onEvent: (candleData: CandleDataInterface, exchange: string, asset: string) => any,
                 candleData: CandleDataInterface, exchange: string, asset: string) {

        if (!this._currentTradeIds[candleData.getGranularity()]) {
            this._currentTradeIds[candleData.getGranularity()] = [];
        }

        // If candle is already published ignore it
        if (this._candleExistsExistsForTradeIds(TradeUtil.getTradeIdsForList(candleData.getLastTrades()), candleData.getGranularity())) {
            return;
        }

        // Otherwise add to trade ids and publish event
        this._publishCandle(onEvent, candleData, exchange, asset);
    }

    private _candleExistsExistsForTradeIds(tradeIds: string[], granularity: number) {
        for (const tradeId of tradeIds) {
            if (this._currentTradeIds[granularity].indexOf(tradeId) !== -1) {
                return true;
            }
        }
        return false;
    }

    private _publishCandle(onEvent: (candleData: CandleDataInterface, exchange: string, asset: string) => any,
                           candleData: CandleDataInterface, exchange: string, asset: string) {

        const tradeIds = TradeUtil.getTradeIdsForList(candleData.getLastTrades());

        for (const tradeId of tradeIds) {
            if (this._currentTradeIds[candleData.getGranularity()].length === CandleEventMerger.BUFFER_SIZE) {
                this._currentTradeIds[candleData.getGranularity()].shift();
            }
            this._currentTradeIds[candleData.getGranularity()].push(tradeId);
        }

        onEvent(candleData, exchange, asset);
    }

}