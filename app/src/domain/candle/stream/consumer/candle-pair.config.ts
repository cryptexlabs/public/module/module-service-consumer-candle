import {LoggerService} from "@nestjs/common";
import yaml = require("js-yaml");
import fs = require("fs");
import {CandleConsumerModuleExchangesConfig} from "../../config/classes/candle.consumer-module.exchanges-config";
import {CandleConfig} from "./candle.config";

export class CandlePairConfig {

    private readonly _configs: CandleConfig[];

    constructor(configFileLocation: string, logger: LoggerService) {

        this._configs = [];

        if (!fs.existsSync(configFileLocation)) {
            logger.log("Config file does not exist: " + configFileLocation);
            process.exit(1);
        }

        const rawConfig = yaml.safeLoad(fs.readFileSync(configFileLocation, "utf8"));

        const config = new CandleConsumerModuleExchangesConfig(rawConfig.exchange);

        console.log(config.getExchanges()[0].getCandle().getPairs()[0].getId());
        console.log(config.getExchanges()[0].getCandle().getPairs()[0].getGranularityConfig().getSizes()[2].getSize());
        console.log(config.getExchanges()[0].getCandle().getPairs()[0].getGranularityConfig().getSizes()[2].getLength());
        // TODO use exchanges config to create stream service config
    }

    public getConfigs(): CandleConfig[] {
        return this._configs;
    }
}