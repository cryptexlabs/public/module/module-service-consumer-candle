# Cryptex Labs Candle Consumer Service Module

This module reads candle updates from a redis stream and writes those updates to a redis key value store.

## Data structures

### Redis key format
Redis key format for candle list:

```
candles:{EXCHANGE_ID}:{PAIR_ID}:{CANDLE_SIZE}
```

#### Examples:
```
candles:GDAX:BTC-USD:1
candles:GDAX:BTC-USD:5
candles:GDAX:BTC-USD:15
candles:GDAX:BTC-USD:60
candles:GDAX:BTC-USD:240
candles:GDAX:BTC-USD:1440
```

#####Example EXCHANGE_ID (Always All caps. No spaces): 
```
BITMEX
CEX
BINANCE
COINBASEPRO
```

##### Example PAIR_ID (Always all caps. Order of pair matters. Dash separated):
```
LTC-BTC
ETH-USD
XRP-BTC
BTC-USDT
```

##### Example CANDLE_SIZE (Number of minutes for size of candle):
```
1
5
15
60
240
1440
```

### Candle list data structure

#### Example payload
```json
[
  {
    "candle": {
      "open":"3853.36000000",
      "high":"3856.12000000",
      "low":"3847.70000000",
      "close":"3856.12000000",
      "volume":"137.0605768700",
      "time":1546012800000
    },
    "lastTrades": [
      {
        "time": 154601314940,
        "price": "3854.12000000",
        "quantity":"0.392",
        "tradeId":"3dj19d101kd013"
      }
    ]
  }
]
```

#### Candle data structure explanation:
```yaml
candle: "The OHLCV wrapper"
  open: "The first price the pair was sold for"
  high: "The highest price the pair was sold for"
  low: "The lowest price the pair was sold for"
  close: "The last price the pair was sold for"
  volume: "The total amount of the asset that was sold"
  time: "The unix timestamp in milliseconds for when the candle starts"
lastTrades: "A list of trades for the last timestamp timestamp within this candle. It is a list becuase although it is rare it is possible for multiple trades to execute with different trade identifiers and for different prices and volumes at the exact same time. If you want the last trade price you need to either use weighted averaging, min, or max of this list. The times will be identical for this list."
  - time: "The time of the trade. Also the time that the candle was last updated"
    price: "The price that was paid for the asset"
    quantity: "How much of this particular asset was traded"
    tradeId: "A unique identifier for this particular trade. May or may not be sequential"
```

## Configuration
How many candles are in each Redis key is configurable for each Redis key. When your trading instance starts up historical data is pulled in and saved. When the candle limit is reached older candles will be removed while newer candles are added. 

### Setup
TODO Add documentation about how to configure the assets for each trading instance for this module